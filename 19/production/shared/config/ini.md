Please copy production_dummy.ini. Then change content.

```
cp production_dummy.ini production.ini
```

Please copy sudo_dummy.ini. Then change content. Do _not_ use "$" in password.

```
cp sudo_dummy.ini sudo.ini
```
