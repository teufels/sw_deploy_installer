# config valid only for current version of Capistrano
lock '3.5.0'
Dotenv.load

set :application, ENV['CAP_DEPLOY_APPLICATION']
##
## :repo_url
##
## e.g. set :repo_url, 'git@bitbucket.org:teufels/teu_18_ws_installer.git'
##
set :repo_url, ENV['CAP_DEPLOY_THM_CUSTOM_URL']
set :branch, ENV['REVISION'] || ENV['BRANCH_NAME']

namespace :deploy do
  task :composer do
    on roles(:web) do
      within release_path do
        execute :chmod, '-R', '775', './web'
        execute :chmod, '-R', '775', './composer.json'
        execute "echo '#{fetch(:teufels_database_ini)}'"
        execute "echo '#{fetch(:teufels_stage).inspect}'"
        execute 'wget', '-q', '-nc', 'https://getcomposer.org/composer.phar'
        execute "#{fetch(:teufels_php_cli_bin)}", "-c", "./php.ini" , './composer.phar', 'install', '--no-dev', '--no-ansi', '--no-interaction'

        ####################################################################################################################
        ##
        ## Uncomment if not using PHP with CGI !!!
        ##
        #execute :mv, "./web/index.php", "./web/_index.php"
        #execute :cp, "./vendor/typo3/cms/index.php", "./web"
        ##
        ####################################################################################################################

        execute :ln, "-s", "#{fetch(:teufels_database_ini)}", "./web/typo3conf/server.ini"
        execute :ln, "-s", "#{fetch(:teufels_sudo_ini)}", "./sudo.ini"

        if fetch(:teufels_stage) == "staging"
            execute :ln, "-s", ".htaccess_staging", "./web/.htaccess"
        else
            execute :ln, "-s", ".htaccess_production", "./web/.htaccess"
        end

        ####################################################################################################################
        ##
        ## Add shared folders below !!!
        ##
        execute :ln, "-s", "#{fetch(:teufels_fileadmin)}", "./web/fileadmin"
        execute :ln, "-s", "#{fetch(:teufels_uploads)}", "./web/uploads"
        ##
        ####################################################################################################################

        execute :sh, "./install_typo3.sh"

        # ./temp_defaults-extra-file.cnf created has been created in ./install_typo3.sh
        definition_exist = test("[ -f #{fetch(:teufels_database_dump_definition)} ]")
        data_exist = test("[ -f #{fetch(:teufels_database_dump_data)} ]")
        if definition_exist == true
            execute :mysql, "--defaults-extra-file=./temp_defaults-extra-file.cnf", "--default-character-set=utf8", "<", "#{fetch(:teufels_database_dump_definition)}"
            #run "rm -rf #{fetch(:teufels_database_dump_definition)}"
            execute :rm, "-f", "#{fetch(:teufels_database_dump_definition)}"
            execute :echo, "mysql: definition imported"
        else
            execute :echo, "mysql: no definition imported"
        end
        if data_exist == true
            execute :mysql, "--defaults-extra-file=./temp_defaults-extra-file.cnf", "--default-character-set=utf8", "<", "#{fetch(:teufels_database_dump_data)}"
            #run "rm -rf #{fetch(:teufels_database_dump_data)}"
            execute :rm, "-f", "#{fetch(:teufels_database_dump_data)}"
            execute :echo, "mysql: data imported"
        else
            execute :echo, "mysql: no data imported"
        end

        execute :sh, "./install_typo3_extensions.sh"
        execute :sh, "./install_npm_packages.sh"
        execute "#{fetch(:teufels_node_modules_bin)}/gulp"

        execute :rm, "-f", "./temp_defaults-extra-file.cnf"

        ####################################################################################################################
        ##
        ## Uncomment if using PHP with opcache enabled !!!
        ##
        #execute "#{fetch(:teufels_php_bin)}", "-c", "./php.ini" , './opcache_reset.php'
        ##
        ####################################################################################################################

        execute :touch, "./web/typo3conf/ENABLE_INSTALL_TOOL"
      end
    end
  end
  after :updated, 'deploy:composer'
end
